import re
import pdftotext

# Definitions
list_mounths = [
    "ENE",
    "FEB",
    "MAR",
    "ABR",
    "MAY",
    "JUN",
    "JUL",
    "AGO",
    "SEP",
    "OCT",
    "NOV",
    "DIC"
]


class HystoryAbstracter:
    """
    Scraps into a given pdf path and gets the final values of the history
    Assumptions:
    1. The given pdf is suitable CFE bill
    2. The kind of contract scheme of CFE  match with a monthly bill
    """
    # ====================================================================
    # CONSTRUCTOR
    # ====================================================================

    def __init__(self, location):
        # imputs setup
        self.__path = location
        # private properties setup
        self.__section = self.__section_processor()
        self.__output = self.__output_processor()

    # ====================================================================
    # PROPERTIES ACCESORS
    # ====================================================================

    @property
    def Section(self):
        """All the history section in the pdf"""
        return self.__section

    @property
    def Output(self):
        """Returns the output dicctionary"""
        return self.__output
    # ====================================================================
    # Variable definitions
    # ====================================================================

    def __section_processor(self):
        with open(self.__path, 'rb') as pdf:
            pdf = pdftotext.PDF(pdf, '-enc utf-8')
            return pdf
    # ====================================================================
    # METHODS
    # ====================================================================

    def __output_processor(self):
        output = {}
        for mounths in list_mounths:
            mounth_patterns = '(?P<mounth>{mounth})(\s+)(?P<year>\d*)(\s+)(?P<high_demand>\d*)(\s+)(?P<total_consumption>\d*.\d*)(\s+)(?P<power_factor>\d*.\d*)(\s+)(?P<charge_factor>\d*)(\s+)(?P<average_price>\d*.\d*)'.format(
                mounth=mounths
            )
            mounth_len = sum(1 for _ in re.finditer(mounth_patterns, self.__section[1]))
            # Sums the amount of iterations
            mounth = re.finditer(mounth_patterns, self.__section[1])
            # mounth = [any for any in mounth]
            mounth = list(mounth)
            # ===========================================================================
            # If there are more than one coincidence
            # ===========================================================================
            if mounth_len > 1:
                # print('there are {}'.format(mounth_len), 'coincidences')
                # If there are more than one coincidence
                year = 0
                # Local variable to test coincidences
                index = 0
                for coincidence in mounth:
                    # Cycle to sum every year
                    year = year + int(coincidence.group('year'))
                    index = index + 1
                    if index == mounth_len:
                        index = int(coincidence.group('year'))
                        year = year / mounth_len
                        # print('######the sum of years is {}'.format(year))
                        # print('######the denominator is {}'.format(index))
                        # Reuses index to be equal as any year in the las iteration
                # ===========================================================================
                # If the coincidences does not belog to the same year
                # ===========================================================================
                if year != index:
                    # print('year',year, ' and index:', index)
                    # print('entro, el año es distinto, el mes es {mounth}'.format(mounth=mounths))
                    # Appends the output in case the coincidences belog to different years
                    for coincidence in mounth:
                        key = mounths + '(' + coincidence.group('year') + ')'
                        output[key] = coincidence.groupdict()
                # ===========================================================================
                # If coincidences belongs to the same year
                # ===========================================================================
                else:
                    # print('entro, el año es el mismo para el mes {mounth}'.format(mounth=mounths))
                    # Appends the properly ouput
                    year = 0
                    high_demand = 0
                    total_consumption = 0
                    power_factor = 0
                    charge_factor = 0
                    average_price = 0
                    for coincidence in mounth:
                        # print('entró a modificar los valores')
                        # Set year
                        year = coincidence.group('year')
                        # Set high_demand
                        if float(coincidence.group('high_demand')) > high_demand:
                            high_demand = float(coincidence.group('high_demand'))
                        # Set total_consumption
                        total_consumption = total_consumption + float(coincidence.group('total_consumption').replace(',', ''))
                        # Set power_factor
                        if float(coincidence.group('power_factor')) > power_factor:
                            power_factor = float(coincidence.group('power_factor'))
                        # Set charge_factor
                        if float(coincidence.group('charge_factor')) > charge_factor:
                            charge_factor = float(coincidence.group('charge_factor'))
                        # Set average_price
                        if float(coincidence.group('average_price')) > average_price:
                            average_price = float(coincidence.group('average_price'))
                    output[mounths] = {
                        'mounth': mounths,
                        'year': year,
                        'high_demand': high_demand,
                        'total_consumption': total_consumption,
                        'power_factor': power_factor,
                        'charge_factor': charge_factor,
                        'average_price': average_price
                    }
            # ===========================================================================
            # If there are just a single coincidence
            # ===========================================================================
            else:
                for coincidence in mounth:
                    output[mounths] = coincidence.groupdict()
        return output
