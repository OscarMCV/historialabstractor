
Abstracción de la clase: Monomio
=========

### **¿Cuál es el nombre de la clase?**

HistoryAbstracter

### **¿Qué representa la clase?**

Un abstractor que filtra pdfs para pbtener información de sus historiales

### **¿Qué información es relevante respecto de un objeto de esta clase?**

Es necesario conocer:
* El output corregido del historial de todos los meses (por si se necesita)
**Nota:
Para optimizar memoria, la sección se iba a eliminar pero por cuestiones de pruebas se estableció como una propiedad accesible

### **¿Qué tipo de dato es cada propiedad?**

Todas las propiedades son númericas e inmutables

### **¿Qué acciones puede realizar un objeto de esta clase?**

Puede generar un output con la información requisada por el problema

### **¿Qué dato necesito para cada método y qué devuelve?**
Ninguno de los métodos necesita datos.


### **¿Qué dato necesito para crear un objeto?**
Es estríctamente necesario:
* Proporcionar la dirección del archivo que coincida con el formato de cobro mensual

### **¿Puedo crear un objeto sin que me den datos?**
* No



#  Nombre: HistoryAbstractor
# Variables:
* outout : Diccionario
* Section : Cadena de caracteres
# Métodos
* Constructor(pad_variable : string)
* output(): diccionario

Nota: El output se incluye como parámetro por cuestiones de prueba


 <p> {{nombre}} {{apellido}}</p>